require "./spec_helper"

def db_count_of(table_name : String)
  db = DB.open(ENV["DATABASE_URL"]? || "postgres://postgres:@localhost:5432/slate_test")

  begin
    db.scalar("select count(*) from #{table_name}")
  ensure
    db.close
  end
end

def fixture_count_of(fixture_name : String)
  load_fixture("spec/fixtures/#{fixture_name}.yml").size
end

def load_fixture(fixture_path : String)
  YAML.parse(File.read(fixture_path))
end

describe Slate do
  describe ".load_fixtures" do
    context "not given specific fixtures to load" do
      it "loads and dumps yml files from 'spec/fixtures'" do
        Slate.load_fixtures

        db_count_of("users").should eq fixture_count_of("users")
        db_count_of("products").should eq fixture_count_of("products")
      end
    end

    context "given specific fixture files" do
      users_fixture_path = "spec/fixtures/users.yml"
      fixture_files = [users_fixture_path]

      it "loads the specified yaml files" do
        Slate.load_fixtures(fixture_files)

        db_count_of("users").should eq fixture_count_of("users")
      end

      it "doesn't load other yaml files" do
        Slate.load_fixtures(fixture_files)

        db_count_of("products").should eq 0
      end
    end
  end
end
