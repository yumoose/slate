require "spec"
require "pg"

require "../src/slate"

Spec.before_each do
  db = DB.open(ENV["DATABASE_URL"]? || "postgres://postgres:@localhost:5432/slate_test")

  begin
    # create a new database table to spec against
    db.exec(
      "DROP TABLE IF EXISTS users;"
    )

    db.exec("CREATE TABLE users(
      id BIGSERIAL PRIMARY KEY,
      name VARCHAR (50) UNIQUE NOT NULL,
      age INT,
      verified BOOLEAN,
      verified_at TIMESTAMP
    );")

    db.exec(
      "DROP TABLE IF EXISTS products;"
    )

    db.exec("CREATE TABLE products(
      id BIGSERIAL PRIMARY KEY,
      name VARCHAR (50) UNIQUE NOT NULL,
      description VARCHAR (200),
      price DECIMAL
    );")
  ensure
    db.close
  end
end
