# slate

A barebones fixture library for supporting specs in crystal projects

## Installation

1. Add the dependency to your `shard.yml`:
```yaml
development_dependencies:
  slate:
    gitlab: yumoose/slate
```
2. Run `shards install`

## Usage

Require Slate and load fixtures in the spec helper for your project
```crystal
# spec/spec_helper.cr

require "slate"

Spec.before_each do
  Slate.load_fixtures
end
```

Or if your project supports transactions, with Jennifer ORM for example,
```crystal
# spec/spec_helper.cr

require "slate"

Slate.load_fixtures

Spec.before_each do
  Jennifer::Adapter.adapter.begin_transaction
end

Spec.after_each do
  Jennifer::Adapter.adapter.rollback_transaction
end
```

## Contributing

1. Fork it (<https://gitlab.com/yumoose/slate/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Jordane Lew](https://github.com/your-github-user) - creator and maintainer
